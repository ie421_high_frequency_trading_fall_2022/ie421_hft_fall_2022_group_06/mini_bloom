import pandas as pd
import numpy as np

orderBookDF = pd.read_csv("20220801_book_updates.csv")
tradeDF = pd.read_csv("20220801_trades.csv")

orderBookDF.loc[:'MESSAGE_ID']
tradeIDs = tradeDF["MESSAGE_ID"].tolist()
# tradeDF.loc[:'MESSAGE_ID']

# trade_occurrences = df = pd.concat(orderBookDF, axis=0)

# tradeDF.iloc[1,2]
# print(tradeDF)
previous_message_ID = 0
for i in list(tradeDF):
    currentMessageID = tradeIDs[0]
    if currentMessageID > previous_message_ID and currentMessageID < int(tradeDF[i][3]):
        df_new = pd.DataFrame(columns=tradeDF.columns)
        for idx in range(len(tradeDF)):
            df_new = df_new.append(tradeDF.iloc[idx])
            for _ in range(len(tradeIDs)):
                df_new=df_new.append(pd.Series(), ignore_index=True)
    previous_message_ID = tradeDF[i][1]
    print(i)
    print()

def NAN_rows(df, rows, cols):
    row = df.shape[0]
    x = np.empty((rows,cols,)) # 3 empty row and 2 columns. You can increase according to your original df
    x[:] = np.nan
    df_x = pd.DataFrame( columns = tradeDF.columns)
    for i in range(row):
        temp = np.vstack([df.iloc[i].tolist(),x])
        df_x = pd.concat([df_x, pd.DataFrame(temp,columns = tradeDF.columns)], axis=0)
        
    return df_x