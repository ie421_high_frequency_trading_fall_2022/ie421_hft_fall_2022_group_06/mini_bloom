// CppFlow headers
#include <cppflow/ops.h>
#include <cppflow/model.h>



int main() {

    //load the model
    // cppflow::model model("saved_model_folder");

    // // Load an image
    // auto input = cppflow::decode_jpeg(cppflow::read_file(std::string("image.jpg")));

    // // Cast it to float, normalize to range [0, 1], and add batch_dimension
    // input = cppflow::cast(input, TF_UINT8, TF_FLOAT);
    // input = input / 255.f;
    // input = cppflow::expand_dims(input, 0);

    // // Run
    // auto output = model(input);

    // // Show the predicted class
    // std::cout << cppflow::arg_max(output, 1) << std::endl;
    // // C++ headers
    // #include <iostream>


    auto input_1 = cppflow::fill({10, 5}, 1.0f);
    auto input_2 = cppflow::fill({10, 5}, -1.0f);
    cppflow::model model(std::string(MODEL_PATH));

    auto output = model({{"serving_default_my_input_1:0", input_1},
                         {"serving_default_my_input_2:0", input_2}},
                        {"StatefulPartitionedCall:0",
                         "StatefulPartitionedCall:1"});

    std::cout << "output_1: " << output[0] << std::endl;
    std::cout << "output_2: " << output[1] << std::endl;
    return 0;
}

