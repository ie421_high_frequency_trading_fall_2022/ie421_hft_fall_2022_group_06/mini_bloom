# Group 6 IE High Frequency Trading Technology Final Report

## Teammates

**Naren Alluri**

**Eric Liro**
- I am Eric Liro, a Master's Student in Computer Science at the University of Illinois at Urbana-Champaign. I am graduating in December 2022, and have taken courses in distributed systems, security, machine learning, databases, and communication networks. I have experience working in industry as a Software Development Intern at Amazon and Panasonic. My skills include multiple high-level languages such as Python, C++, Java, Javascript, and Kotlin. I also am experienced with frontend development and cloud platforms. My interests include infrastructure, finance, and cloud.

- My LinkedIn is located here: https://www.linkedin.com/in/eric-liro/

**Haojiong "John" Zhang**
- I am John Zhang, an Undergraduate student studying Computer Engineer at the University of Illinois at Urbana-Champaign. I am currently working as an undergrad researcher on transformer-based models. I have taken courses in security, machine learning, communication networks, and operating systems. 
My LinkedIn is located https://www.linkedin.com/in/haojiong-zhang-4b68881aa/

**Jiaru "Rubin" Zou (jiaruz2@illinois.edu)**
- I am Rubin Zou, a junior majoring in Computer Engineering at the University of Illinois ar Urbanna-Champaign. I am going to graduate on May 2024. I have taken courses related to deep leanring/ML, optimization, communication networks, and algorithms design. My skills include python(PyTorch, TensorFlow), C++/C, AWS, Azure, and Linux OS. I am interested in model quantizations, machine learning algorithm design, and model accerelation areas. 
My LinkedIn is located here: https://www.linkedin.com/in/jiaru-zou-67434a21a/
## **Introduction**

##### **Project Description:**

This is a semester long project for IE 421 - High Frequency Trading Technology instructed by **Professor David Lariviere.** The main goal of the project is to evaluate the performance on training ML models using multiple acceleration infrastructures (Apple's M1, M2 chips, Google Colab's Nvidia's A100, T4 GPU, local Nvidia 1660ti, Goole Cloud Platform, NCSA HAL Cluster) and test the high accuracy training and low cost inference. We also tested the differences in performance between pytorch and tensorflow implementation. 

###### **Main Objectives:**

- To design a parser and search engien to create a dataset for model training, validating, and testing.
- To develop a computationally intensive model for testing large amounts of High Frequency Trading (HFT) data
- To have this model satisfy technical requirements allowing it to be hardware accelerated
- To test this model by using multiple hardware accelerators, including on the cloud

###### **Tools and sources for developing the data, model and benchmarking:**

***Data source**:*

The main data source for this project is through IEX, an exchange which generates vast amounts of HFT data. We have adapted Professor Lariviere’s IEX data downloader and parser for this project.

***Strategy Development**:*

We researched multiple models for testing the data and decided on using Long Short-Term Memory (LSTM) as the model. This serves as a computationally intensive model based on time-series data that can be hardware accelerated and can provide an output loss. This loss can be used for comparison purposes, as well as the time it takes to run the model.

***Visualizing**:*

We executed the LSTM model on two separate Machine Learning (ML) kernels; these being PyTorch and TensorFlow. Both of these kernels can provide hardware acceleration, which we ran locally on various machines, as well as on the cloud. The benchmarking resulted in loss scores and time to completion for the model testing. 

## **Technologies**

### **Programming Languages:**

- *Git bash:*
  - Git Bash is an application for Microsoft Windows environments which provides an emulation layer for a Git command line experience
  - We have used it for feeding the data into git and for automation.
- *Python:*
  - We have used Python for developing our strategy initially and debug the issues we faced in the strategy.
  - For visualizing of the back test output, we have used python Matplotlib package for plotting the data.

### **Software:**
- Jupyter notebook:
  - It is a light weight dev tool for Python which we have developed both PyTorch and TensorFlow LSTM Model’s in.
- GitLab:
  - We have used Gitlab to store our models and data for the project.
- VS code:
  - We have used VS code to run local code for the project.
- Google Colab
  - We used Colab to collaborate with one another for developing the model and editing it in real-time.

### **Frameworks:**
- PyTorch:
  - PyTorch is an ML framework that is free and open-source which allows for tensor computing with acceleration.
- TensorFlow:
  - TensorFlow is an AI and ML framework that can provide tensor processing allowing for accelerated computing.

### **Packages:**
Below are the python packages we have used to implement the LSTM model
- Numpy
- Pandas
- Matplotlib
- Datetime
- Warnings
- Argparse
- Glob
- Google.Colab
- Zipfile
- Os


### Refencences:
Special thanks to the LSTM pages on Pytorch:
[https://pytorch.org/docs/stable/generated/torch.nn.LSTM.html](https://pytorch.org/docs/stable/generated/torch.nn.LSTM.html)

and TensorFlow:
[https://www.tensorflow.org/api_docs/python/tf/keras/layers/LSTM](https://www.tensorflow.org/api_docs/python/tf/keras/layers/LSTM)

### **Instruction for Executing Project:**

Running our models depends on the choice of hardware accelerator and cloud platform. All require the following step, however:
1. Clone the repository and download the data

To run on Google Cloud Platform (GCP), follow these steps:

2. Create a Google Colab account
3. [Optional] Purchase a premium subscription (this provides better hardware accelerators)
4. Upload the GCP PyTorch and TensorFlow LSTM .ipynb files to Google Colab
5. Choose the appropriate settings for whether wanting to accelerate (either GPU or TPU) or not
6. Run each cell in order from top to bottom

If wanting to run locally, follow steps 2 through 4 in the GCP instructions and then:

5. Choose the offline setting to use local components rather than online ones
6. Run each cell in order from top to bottom

To run on Amazon Web Services (AWS), follow these steps:

2. Create an AWS account
3. Go to Sagemaker and request an instance limit increase for each accelerate instance
4. Once approved and processed, spin up instances and upload the AWS respective .ipynb LSTM files for PyTorch and TensorFlow
5. Create an S3 bucket and upload the data to that bucket
6. Run each cell in order from top to bottom

To run on NCSA HAL or Campus cluster, please register the accound with the following reference link and run the model on the clusters:

1. HAL: [https://wiki.ncsa.illinois.edu/display/ISL20/HAL+cluster]
2. Campus Cluster:[https://campuscluster.illinois.edu/resources/docs/start/]
## **Description of Project:**

### LSTM Model:
Long Short-Term Memory (LSTM) is a type of Recurrent Neural Network (RNN) that is capable of learning and remembering long-term dependencies in sequential data. RNNs are neural networks that have a "memory" that allows them to process sequences of inputs, such as text, speech, or time series data. However, traditional RNNs are not able to effectively handle long-term dependencies due to the vanishing and exploding gradients problem.

LSTMs were designed to overcome this problem by introducing a new kind of neuron called a "memory cell" that can retain information for a longer period of time. The memory cell is a simple unit that can store and retrieve information, and it is connected to three different gates: an input gate, an output gate, and a forget gate. These gates are used to control the flow of information into and out of the memory cell.

![block-diagram](images/neuron.png)
The basic type of an artificial nueron in LSTM model in shown above.

The input gate regulates the flow of new information into the memory cell, while the forget gate controls the removal of outdated information from the memory cell. The output gate determines the value that is passed to the next layer of the neural network. By controlling these gates, LSTMs are able to selectively remember and forget information, which allows them to effectively learn long-term dependencies.

![block-diagram](images/LSTM_Cell.svg.png)
An LSTM Cell that Processes Data Sequentially and Keeps a Hidden State

![block-diagram](images/multilayer.png)
A multilayer feed-forward neural network with one input layer, twohidden layers, and an output layer. Using neurons with sigmoid threshold functions, these neural networks are able to express non-linear decision surfaces.

LSTMs have been used in a wide range of applications, including natural language processing, speech recognition, and time series forecasting. They have also been used in sequential decision-making tasks, such as controlling robots and autonomous vehicles.

One of the benefits of LSTMs is that they are able to handle sequences of varying lengths and can be trained using standard backpropagation algorithms. Additionally, LSTMs can be stacked to form deeper architectures, which allows them to learn even more complex patterns in the data.

Despite the many advantages of LSTMs, they also have some limitations. One of the main challenges with LSTMs is that they can require a large amount of data to train effectively, which can make them impractical for some applications. Additionally, LSTMs can be computationally expensive to train and use, which can be a problem for some applications.

Effectively, LSTM is a powerful type of RNN that is capable of learning and remembering long-term dependencies in sequential data. It has been widely used in various applications and has shown promising results. However, LSTMs also have some limitations, such as the need for large amounts of data and high computational costs. Despite these limitations, LSTMs remain a popular choice for many sequential learning tasks.

The model is used to evaluate the data and determine losses over the course of multiple epochs. Specifically, both PyTorch and TensorFlow offer LSTM functionality in each of their respective frameworks allowing for the analysis of our data to be performed. However, there still needed to be hyperparamter tuning in order to result in relevant results and useful information. 
### **Accelerators**
#### **Google Colab:**
- Standard: NVIDIA T4 Tensor Core with 789GB [specs](https://www.nvidia.com/en-us/data-center/tesla-t4/) or K80 with 1197GB [specs](https://www.nvidia.com/en-gb/data-center/tesla-k80/)
- Premium: NVIDIA V100 [specs](https://www.nvidia.com/en-us/data-center/v100/)/ A100 Tensor Core [specs](https://www.gigabyte.com/Solutions/nvidia-a100)
The premium also comes with TPU v4 with currently supports TensorFlow, Pytorch, and JAX with setup [here](https://cloud.google.com/tpu/docs/quick-starts)

NVIDIA Tensor Cores are specialized hardware components designed for deep learning and artificial intelligence workloads. They were first introduced with NVIDIA's Volta architecture and are also available in their subsequent Turing and Ampere architectures.

Tensor Cores accelerate matrix multiplication operations commonly used in deep learning, allowing for faster training and inference times. They can perform mixed-precision operations, which allow for more efficient use of memory and faster training times.

In mixed-precision operations, the Tensor Cores use lower-precision (such as half-precision) calculations for most of the matrix multiplication, but then use higher-precision (such as single-precision) calculations for the final result. This reduces memory usage and allows for faster computations.
#### **Nvidia GPUs:**
Locally we were able to run with the NVIDIA GTX 1660 Ti [specs](https://www.asus.com/motherboards-components/graphics-cards/dual/dual-gtx1660ti-o6g/techspec/) (There might be performance decrease given it was purchased four years ago)
The newer NVIDIA GTX cards come with Tensor Cores which were made specifically to accelerate machine learning however the 1660 Ti does not have these chips

#### **Apple M1/M2:**
M1:
It includes an 8-core CPU with 4 high-performance cores and 4 high-efficiency cores, an 8-core GPU, and a 16-core Neural Engine. The Neural Engine is specifically designed for machine learning tasks and is capable of performing up to 11 trillion operations per second. It includes specialized hardware for matrix multiplication and convolution operations commonly used in deep learning.

In addition, the M1 includes a unified memory architecture that allows the CPU, GPU, and Neural Engine to access the same pool of memory, providing faster data transfer and reducing the need to copy data between different memory banks. Overall, the M1's specs make it a capable processor for machine learning tasks, particularly for small to medium-sized models. However, for larger models, it may be outperformed by dedicated GPUs or specialized processors such as the NVIDIA Tensor Cores.

M2:
The CPU and GPU is 20% and 28% faster compared to the M1 compared on ResNet50 and BERT for one epoch

M2 improves upon the M1 even further with an 18 percent faster CPU, a 35 percent more powerful GPU, and a 40 percent faster Neural Engine with process speeds up to 15.8 trillion operations per second. The chip delivers 50 percent more memory bandwidth compared to M1 and up to 24 GB of fast unified memory
#### **NCSA HAL:**
HAL system provides GPU profile functionality via NVIDIA Nsight System CLI and NVIDIA Nsight Compute CLI. Users can generate the profile result files on HAL system and then download them to their local machine to visualize. Hardware equipements in HAL:
* 16 IBM AC922 nodes
* IBM 8335-GTH AC922 server
* 2x 20-core IBM POWER9 CPU @ 2.4GHz
* 256 GB DDR4
* 4x NVIDIA V100 GPUs
* 5120 cores
* 16 GB HBM 2
* 2-Port EDR 100 Gb/s IB ConnectX-5 Adapter
* 1 IBM 9006-22P storage node
* 72TB Hardware RAID array
* NFS
* 3 DDN GS400NVE Flash Arrays
* 360 TB usable, NVME SSD-based storage
* Spectrum Scale File System

NVIDIA Nsight Systems is a low overhead performance analysis tool designed to provide insights developers need to optimize their software. Unbiased activity data is visualized within the tool to help users investigate bottlenecks, avoid inferring false-positives, and pursue optimizations with higher probability of performance gains. Users will be able to identify issues, such as GPU starvation, unnecessary GPU synchronization, insufficient CPU parallelizing, and even unexpectedly expensive algorithms across the CPUs and GPUs of their target platform. In addition, NVIDIA Nsight Compute CLI (nv-nsight-cu-cli) provides a non-interactive way to profile applications from the command line. It can print the results directly on the command line or store them in a report file. It can also be used to simply launch the target application and later attach with NVIDIA Nsight Compute or another nv-nsight-cu-cli instance.
#### **Campus Cluster:**
The CampusCluster is a link between many different servers maintained by various colleges, departments, labs and individuals at the University of Illinois at Urbana-Champaign. The central idea is as follows: Many different groups need server cycles, but most of them don’t always need that much power. In fact, most of the time those servers are inactive! The campuscluster is a server network that not only allows users to run programs on their own servers, but also allows users to request cycles from other people’s inactive servers.

![block-diagram](images/cc.png)
Big idea for using the CC: you submit jobs to queues not servers. So instead of telling the server that you are logged into to run prog.exe, you instead have to tell a queue that you want a server with XYZ hardware to run prog.exe. The queue will wait untl the server has completed the jobs before you to send it your job.

Each server is connected to two types of queues, a primary and secondary. The primary queue is specific to that server and access is typically limited to the individuals/groups that own that server. The server itself priortizes taking jobs from the primary queue basically giving its owners dibs on the server time. This gives groups an incentive to buy and connect servers to the CC since their server will always priotize their work.

### **Benchmarking Procedure and Results:**

#### **Procedure:**
The procedure for benchmarking the model is relatively straightforward and repetitive. Starting with Google Colab, since they have default Jupyter Notebook functionality, they provide a means of testing directly on their GPUs and TPUs, allowing for benchmarking to occur. Additionally, they have packages that can link to Google Drive, which provides 15 GB of free storage, which can allow for the data to be stored there with ease. This connectivity then allows the user to run through the notebook, run the cells, and output results while changing very little.

However, with Google, it isn't exactly clear the exact computer components that are being worked with, and while they claim a GPU is accelerated the computation even on the free version of Colab, it is quite slow compared to the paid version, taking almost 45 minutes per epoch for the run. This is somewhat akin to testing locally, where laptop graphics processing units aren't nearly as fast as cloud platform ones. However, once opting for the paid version, the per epoch time is much faster.

While Google Colab provides ease, AWS offers many more instance types, at the cost of more complication and generally being more expensive. For instance, one cannot read the data through just Sagemaker, requiring spinning an S3 bucket and connecting to this within the instance itself. This is done with relative ease, however, similar to how Google Colab allows for connectivity to S3. It should be noted that Sagemaker does not allow connectivity to Google Drive, somewhat understandably. 

Afterwards, it is also unfortunately prohibitive to spin an instance on Sagemaker in the Ohio region, which is closest to Illinois. The default instance allowance is 0, meaning any instance that one is interested in must go through a service level agreement to increase the limit beyond 0. This is somewhat cumbersome, because it takes multiple days for approval and processing, resulting in lost time. Due to this, these preliminary results feature only three accelerated instance types using both PyTorch and TensorFlow kernels. However, once the instances are spun, they follow the same procedure as Google Colab; using a typical .ipynb Jupyter notebook resulting in ease of running and waiting. After waiting an appropriate amount of time, the results are presented.

#### **Results:**

| Platform | Instance Type | Kernel | Price ($/hour) | vCPU | Memory | Epoch Count | Batch/Hidden Layer Size | Time to Completion (s) | Final Loss | Mean Absolute Error |
| ----------- | ----------- | ----------- | ----------- | ----------- | ----------- | ----------- |  ----------- | ----------- | ----------- |  ----------- |
| AWS   | ml.g4dn.xlarge | TensorFlow | 0.736 | 4 | 16 GiB | 10 | 32 | 4627 | 0.0000052 |  0.0013 |
| AWS   | ml.g4dn.xlarge | PyTorch | 0.736 | 4 | 16 GiB | 10 | 32 | 4465 | 1.421 | N/A |
| AWS   | ml.g4dn.2xlarge | TensorFlow | 0.94 | 8 | 32 GiB | 10 | 32 | 4219 | 0.0000052 | 0.0013 |
| AWS   | ml.g4dn.2xlarge | PyTorch | 0.94 | 8 | 32 GiB | 10 | 32 | 4460 | 1.096 | N/A |
| AWS   | ml.g4dn.4xlarge | TensorFlow | 1.505 | 16 | 64 GiB | 10 | 32 | 4162 | 0.0000050 | 0.0013 |
| AWS   | ml.g4dn.4xlarge | PyTorch | 1.505 | 16 | 64 GiB | 10 | 32 | 4411 | 0.883 | N/A |
| GCP   | TPU | TensorFlow | 0 | 4 | 12 GiB | 1 | 32 | 3758 | 0.0000594 | 0.0024 |

## **Conclusion:**
Accelerated computing for machine learning is a necessary procedure to hasten the time it takes to run models. Furthermore, there is certainly, at the very least, an academic market for implemeting machine learning in the high-frequency trading realm. However, there isn't much insight into how various accelerated computing instances affect the output speeds and results of ML based models for HFT. This project sought to provide preliminary results for thus, creating more talking points into how the cloud can be used for HFT machine learning models. 

It is of particular interest to compare different cloud platforms between one another, with the main three being AWS, GCP, and Azure, as well as testing locally to see what the cost-benefit analysis is of spending more on instances and if it provides better and faster results. For example, if an accelerated instance costs twice as much as another but only provides a 10% increase in speed, then it is certainly not worth the higher price. This is in fact what the preliminary results show. It can be seen from the table that the 4xlarge instance costs approximately twice as much as the xlarge one, but only provides a few minutes of faster analysis. Interestingly though, this also provides lower losses across the board for both PyTorch and Tensorflow, which may be of particular interest for accurate results. If one seeks the most minimal losses, then evidently more expensive instances do in fact have some benfit toward this, as well as providing faster results. While these results are preliminary with only a single run each at 10 epochs, they do provide some insight into what other accelerated computing types can provide. 

## **Postmortem Summary:**

### Jiaru "Rubin" Zou
#### 1. What did you specifically do individually for this project?
- I researched current machine learning / deep learning models in gradient boosting of order book modeling.
- I collected the dataset using IEX dateset parser and wrote a search and parse engine that can bin and normalize data into a single output dataframe/csv.
- I designed an optimized C++ wrapper that deploys a Python(TensorFlow, PyTorch) based model runnable in a faster C++ environment.
- I trained and tested the LSTM model on the NCSA Hardware-Accelerated Learning (HAL) cluster and UIUC CSL campus cluster and benchmarked our Python and TensorFlow models under different acceleration environments.
- I converted the LSTM model to GCP and TPU and benchmarked it.
- I converted the model and trained and tested using Apple M1 and Nvidia GeForce RTX 3060.
- I wrote the vagrant file for model task evaluation.
- I collected the data of the testing results and made a dashboard for data visualization of the result.

#### 2. What did you learn as a result of doing your project?
-  I gained experience in raw dataset processing, specifically how to create and separate training, validation, and testing data based on a bunch of integrated data sources. 
- I learned how to design and deploy a machine-learning model and finetune the model on a specific dataset for better model performance. Specifically, I gained a better understanding of the  LSTM model and how it can be applied to time-series data such as order book modeling. I also learned how to train and evaluate these models, as well as how to tune hyperparameters to improve performance.
- I had a better understanding of the concept of gradient boosting and how it can be used in financial modeling. I also learned about different techniques for boosting and how to apply them to order book data.
- I also gained experience with cloud computing by testing the model's performance on different accelerations such as GCP. This involved learning how to deploy the ML model in a cloud environment and how to use different tools and services to optimize performance. 
- Overall, working on this project provided me with a range of valuable skills and insights into data processing, machine learning, cloud computing, and financial modeling. It helped me develop my problem-solving and analytical skills, which can be valuable in the area of high frequency trading.

#### 3. If you had a time machine and could go back to the beginning, what would you have done differently?
- One thing that I want to first do differently is to have a better schedule and preparation before the main progress of the project. During our implementation of the project, we spent lots of time waiting for the AWS credits and assessment of GCP. If we applied these requirements earlier, we will have more time to focus on the project implementation itself. 
- Another thing that I might do differently is to investigate more details of the project setup and focus more on the model training itself. We originally planned to use the C++ wrapper to make a TensorFlow/PyTorch model runnable in different accelerating environments, but it turns out that most accelerating tools can directly run with a Python kernel. Spending more time on the model and testing would be more proper to achieve our project’s goal. 

#### 4. If you were to continue working on this project, what would you continue to do to improve it, how, and why?
- Dataset expansion: One thing that I want to improve in the project is to have a better process for the dataset. Specifically, our dataset now only contains the order-book trading information before July 2022, we can further extend our training data to the data source more closely to have a better performance on detecting current order-book trends. On the other hand, besides the trade order book datasets that we used in this project, we could use other datasets to finetune specific features of our model to implement more advanced implementations such as detecting the relationship between price and trader ID, which is useful in the real world high-frequency trading.
- Model selection: We could use a more suitable model for the project and also extend our project to test different model prediction performances under the same conditions of acceleration. Specifically, we could test the order book data on models such as Fast-RNN, KNN, and VGG16.
- Advanced method: Another thing that we would extend is to use a more optimized and advanced acceleration method on the tasks. One method is to apply the quantization method on our current model to increase time efficiency and save memories on hyperparemeters.  Another way is to re-implement our model using C/C++ and use HLS coding for performance in FPGA board.

#### 5. What advice do you offer to future students taking this course and working on their semester long project. Providing detailed thoughtful advice to future students will be weighed heavily in evaluating your responses.
There are several pieces of advice that I would like to give to students who took this class in the future:
- Make a careful decision on the project you plan to work on since some ideas are creative but hard to implement. Having multiple discussions with Professor David Lariviere for advice on the project ideas is pretty helpful. 
- Schedule the project that you decide to work on as many details as you can. With the development of our project, the tasks that we wish to complete are different than our original thought and there are lots of new challengings waiting for us. Thus, deep thinking about the potential tasks and challenges before starting the project is helpful and efficient for project development. 
- Ask the Professor and classmates frequently about the feasibility of your project during the semester to make sure you are in the right way with your project. For our project, we faced different problems in choosing the acceleration tools and machine learning models, through talking with Professor David and teammates from group 7, we gained a lot in the understanding of cloud computing and I-O based comparison.
- Listen carefully to the lectures since the lecture content will cover lots of expertise that might take place in your project. 


### Eric Liro
#### 1. What did you specifically do individually for this project?
- I looked into graphics card acceleration for machine learning instances
- I compiled the acceleration information into a single document for reference later
- I looked into cloud platforms and how to accelerate on them, primarily into AWS and GCP
- I acquired the data for the group which includes approximately 1.5 years of three tickers (SPY, QQQ, AAPL)
- I also wrote a data parser with Rubin that can bin and normalize data into a single output dataframe/csv
- Additionally, I created another parser for combining the trades and updates data
- I ported the PyTorch and TensorFlow models over to AWS for Sagemaker use
- I communicated with AWS and set up the instances, as well as ran them to acquire preliminary results of our model
- The entirety of this preliminary report was done by me as well

#### 2. What did you learn as a result of doing your project?
- As a result of doing this project I learned a significant amount about data cleaning/parsing, how to deal with machine learning models, and how to accomplish cloud setup. In terms of data, it is common knowledge that machine learning models deal with and necessitate a tremendous amount, and much of it needs to make sense for the model to output a relatively useful result. Essentially, if you put garbage in, you'll get garbage out. 
- Additionally, dealing with LSTM models in TensorFlow and PyTorch is not straightforward since the shape of the input cells must make sense for the model. This can be difficult to debug as it is not particularly easy to deal with the shapes of the models. Furthermore, GCP and AWS have different PyTorch and TensorFlow kernels, so what worked on GCP would not work on AWS immediately and vice verse. This is somewhat frustrating, since one would expect that writing once and running would be the ideal scenario, but this was not the case.
- Finally, dealing with cloud platforms and the headaches such as spinning instances, connecting to other services, payment for them, and the race against time to simultaneously debug while also wanting to do so as soon as possible so as not to incur additional cost can be frustrating. Once all is handled, it is relatively easy to spin up more instances and perform more analyses, but without that predefined knowledge, it takes time to figure out how to do so. While the cloud is impressive, it is still relatively new when it comes to computers as a whole, and they can still be a bit finicky to deal with.

#### 3. If you had a time machine and could go back to the beginning, what would you have done differently?
- If I had a time machine and could go back, I would certainly start investigating and playing around with the cloud platforms earlier. For instance, we assumed that on-demand services would sincerely mean on-demand at any moment, not after being approved for a limit increase. This slowed the project down and delayed testing unfortunately.
- Additioanlly, I would start donwloading the data earlier, since this took a large bulk of time, and we would also ideally have determined what format the data should be in. We had trouble at first deciding whether we should port the model to take in the IEX csv data, or if we should modify the data format to make it easier for the frameworks to perform LSTM on. This decision would probably have prevented needing to write no fewer than 3 data parsers.
- Finally, it would have been ideal to find a model to use for the project earlier to allow for starting on it earlier. This would be better because we could have finished the kernel writing and gotten to testing faster. Additionally, we already started the project a month later than the non-accelerated groups because our project was changed throughout the semester to align more with HFT technology.

#### 4. If you were to continue working on this project, what would you continue to do to improve it, how, and why?
- To improve the project, there are a few things we can do. First would be to optimize the frameworks to run better LSTM models. Currently, while we have LSTM working on both PyTorch and TensorFlow, it isn't guaranteed that they can even be comparable because the optimizations and hyperparameter tunings may be different. Furthermore, it would be better to have a more streamlined model. Secondly, it would be good to perform more tests on different instances, including Azure as well as local testing. We didn't finish our model in time to test in the basement of Grainger, which provides different graphics cards for work. This would have been interesting, as it would have created more comparisons. Finally, we would expand the tests to include more epoch counts and different hyperparameters. While we were initially thinking we would receive a $100 bonus from AWS, which delayed our simulation start, this never came in. It would be good to increase the epoch count to allow for more tests to occur on the cloud.

#### 5. What advice do you offer to future students taking this course and working on their semester long project. Providing detailed thoughtful advice to future students will be weighed heavily in evaluating your responses.
- One piece of advice I would give future students is to find a project you are passionate about. It is difficult to find motivation for a project you may not be interesting in. Ask questions in class and participate, which will translate to a better understanding of what you may find interesting in HFT technology and what your project can be about. And obviously start as early as possible, but to also be deliberate in the work and make sure you're not finding busy work for yourself but work that will actually be meaningful toward project completion.

### Haojiong "John" Zhang
#### 1. What did you specifically do individually for this project?
- I worked on researching/implementing the following:
- Data normalization:
- Comparing the following methods of data normalization: scaling to a certain range, log scaling, z-score, and clipping
- IO-based Comparison
- Comparing the difference between reading from raw compressed data and decompressed data.
- Comparing the difference between storing in a local SSD hard drive vs external SSD vs external HDD for data I-O
- Creating an LSTM model on TensorFlow
- Creating a copy of the exact model (input size, layers, optimization, loss) using PyTorch
- Creating a new model that turns continuous time series data into discrete tokens which are then regressed upon leveraging transformers
- Benchmarking the models' runtime on my local computers (Nvidia 1660ti, m2 mac, google collab, google collab pro)

#### 2. What did you learn as a result of doing your project?
The M2 was faster than the google collab pro which was running on V100, which was faster than the nvidia 1660 ti, which outperformed the regular google collab subscription running on Nvidia T4. 
The M2 and collab pro were pretty consistent averaging 20 minutes per epoch while the 1660 and regular collab took more than an hour on average to train a single epoch. 
In terms of reading from zipped versus unzipped, the libraries for Python had considerable overhead (compared on the various book data) when reading directly from the zipfile and was very similar in time compared with unzipped + extracting directly. 
In terms of data IO, there wasn't a big difference between reading the book data from a local SSD hard drive and external SSD, but it was considerably faster than reading from HDD drive on the magnitude of seconds. It seems that the data normalization processes is still bottlenecked by the CPU rather than IO.
In terms of the transformer modeling, I was able to start training the transformed dataset using the bert-uncased configs from hugging face on next sentence prediction tasks where after the time series was tokenized, it was artificially segmented into 30s "words" and 10 minute "sentences". The positive next sentence pairs were generated from continuous data segments throughout the day while the negative next sentence pairs were randomly sample from different time frames. Since the transformer model scaled quadratically with the input size (the bert model has input size 512) I was able to train a custom tokenizer using byte pair encoding. I was unable to arrive at any meaningful representation given 


#### 3. If you had a time machine and could go back to the beginning, what would you have done differently?
Not spend so much time figuring out the lstm and should have probably not went down the rabbit hole of transformers. 

#### 4. If you were to continue working on this project, what would you continue to do to improve it, how, and why?
I would focus on improving the performance of the transformer-based model by experimenting with different architectures and hyperparameters.In terms of benchmarking, I would explore running the models on different hardware configurations, including cloud-based services such as Microsoft Azure.

#### 5. What advice do you offer to future students taking this course and working on their semester long project. Providing detailed thoughtful advice to future students will be weighed heavily in evaluating your responses.
For future students taking this course and working on their semester-long project, my advice would be to start early and stay organized.

### Naren Alluri
#### 1. What did you specifically do individually for this project?

#### 2. What did you learn as a result of doing your project?

#### 3. If you had a time machine and could go back to the beginning, what would you have done differently?

#### 4. If you were to continue working on this project, what would you continue to do to improve it, how, and why?

#### 5. What advice do you offer to future students taking this course and working on their semester long project. Providing detailed thoughtful advice to future students will be weighed heavily in evaluating your responses.


## **Appendix:**

### **Screenshots of Results:**

#### ml.g4dn.xlarge:
TensorFlow:
![block-diagram](images/xlarge_tensorflow.PNG)


PyTorch:
![block-diagram](images/xlarge_PyTorch.PNG)


#### ml.g4dn.2xlarge:
TensorFlow:
![block-diagram](images/2xlarge_Tensorflow.PNG)


PyTorch:
![block-diagram](images/2xlarge_PyTorch.PNG)


#### ml.g4dn.4xlarge:
TensorFlow:
![block-diagram](images/4xlarge_TensorFlow.PNG)


PyTorch:
![block-diagram](images/4xlarge_PyTorch.PNG)

#### GCP TPU:
TensorFlow:
![block-diagram](images/GCP_TensorFlow.PNG)

#### Overall Performance:
![block-diagram](images/1_epoch.PNG)

![block-diagram](images/50_epoch.PNG)
