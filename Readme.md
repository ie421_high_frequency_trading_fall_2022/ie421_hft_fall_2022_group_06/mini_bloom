
# **MINI BLOOM**
*** NOTE: This is a quick guidiance and short discription on the project, detialed report can be found in Report.md
## **Project Description:**

This is a semester long project for IE 421 - High Frequency Trading Technology instructed by **Professor David Lariviere.** The main goal of the project is to evaluate the performance on training ML models using multiple acceleration infrastructures (Apple's M1, M2 chips, Google Colab's Nvidia's A100, T4 GPU, local Nvidia 1660ti, Goole Cloud Platform, NCSA HAL Cluster) and test the high accuracy training and low cost inference. We also tested the differences in performance between pytorch and tensorflow implementation. 

## **Main Objectives:**

- To design a parser and search engien to create a dataset for model training, validating, and testing.
- To develop a computationally intensive model for testing large amounts of High Frequency Trading (HFT) data
- To have this model satisfy technical requirements allowing it to be hardware accelerated
- To test this model by using multiple hardware accelerators, including on the cloud

## **Software:**
- Jupyter notebook:
  - It is a light weight dev tool for Python which we have developed both PyTorch and TensorFlow LSTM Model’s in.
- GitLab:
  - We have used Gitlab to store our models and data for the project.
- VS code:
  - We have used VS code to run local code for the project.
- Google Colab
  - We used Colab to collaborate with one another for developing the model and editing it in real-time.

## **Frameworks:**
- PyTorch:
  - PyTorch is an ML framework that is free and open-source which allows for tensor computing with acceleration.
- TensorFlow:
  - TensorFlow is an AI and ML framework that can provide tensor processing allowing for accelerated computing.

## **Packages:**
Below are the python packages we have used to implement the LSTM model
- Numpy
- Pandas
- Matplotlib
- Datetime
- Warnings
- Argparse
- Glob
- Google.Colab
- Zipfile
- Os

**To run on Google Cloud Platform (GCP), follow these steps:**

2. Create a Google Colab account
3. [Optional] Purchase a premium subscription (this provides better hardware accelerators)
4. Upload the GCP PyTorch and TensorFlow LSTM .ipynb files to Google Colab
5. Choose the appropriate settings for whether wanting to accelerate (either GPU or TPU) or not
6. Run each cell in order from top to bottom

If wanting to run locally, follow steps 2 through 4 in the GCP instructions and then:

5. Choose the offline setting to use local components rather than online ones
6. Run each cell in order from top to bottom

**To run on Amazon Web Services (AWS), follow these steps:**

2. Create an AWS account
3. Go to Sagemaker and request an instance limit increase for each accelerate instance
4. Once approved and processed, spin up instances and upload the AWS respective .ipynb LSTM files for PyTorch and TensorFlow
5. Create an S3 bucket and upload the data to that bucket
6. Run each cell in order from top to bottom

**To run on NCSA HAL or Campus cluster, please register the accound with the following reference link and run the model on the clusters:**

1. HAL: https://wiki.ncsa.illinois.edu/display/ISL20/HAL+cluster
2. Campus Cluster: https://campuscluster.illinois.edu/resources/docs/start/
### Refencences:
Special thanks to the LSTM pages on Pytorch:
[https://pytorch.org/docs/stable/generated/torch.nn.LSTM.html](https://pytorch.org/docs/stable/generated/torch.nn.LSTM.html)

and TensorFlow:
[https://www.tensorflow.org/api_docs/python/tf/keras/layers/LSTM](https://www.tensorflow.org/api_docs/python/tf/keras/layers/LSTM)